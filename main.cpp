/**
* papergrabber
* Copyright (C) 2022 Carl Klemm
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 3 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <scipaper/scipaper.h>

#define INCBIN_PREFIX r
#include "log.h"
#include "incbin.h"

INCTXT(ConfigFile, "../user.ini");

static constexpr size_t resultsPerPage = 200;

int main(int argc, char** argv)
{
	Log::level = Log::DEBUG;

	sci_log_set_verbosity(LL_DEBUG);

	if(!sci_paper_init(nullptr, rConfigFileData, rConfigFileSize))
	{
		Log(Log::ERROR)<<"could not init scipaper";
		return 1;
	}

	int id = sci_backend_get_id_by_name("core");
	if(id == 0)
	{
		Log(Log::ERROR)<<"core libscipaper backend not available, but is required for this application";
		return 1;
	}

	char keywords[] = "impedance spectroscopy";
	DocumentMeta queryMeta = {
		.searchText = keywords,
		.backendId = id
	};

	RequestReturn* req = sci_fill_meta(&queryMeta, nullptr, resultsPerPage, 0);
	bool retried = false;
	if(req)
	{
		size_t pages = req->totalCount/resultsPerPage;
		size_t totalCount = req->totalCount;

		Log(Log::INFO)<<"Got "<<totalCount<<" results in "<<pages<<" pages";

		size_t processed = 0;
		for(size_t page = 0; page < pages; ++page)
		{
			if(page != 0)
				req = sci_fill_meta(&queryMeta, nullptr, resultsPerPage, page);
			if(!req)
			{
				if(!retried)
					--page;
				retried = true;
				continue;
			}
			else
			{
				retried = false;
			}

			Log(Log::INFO)<<"Processing page "<<page<<": "<<processed<<" of "<<req->totalCount<<
				" got "<<req->count<<" results this page";
			for(size_t i = 0; i < req->count; ++i)
			{
				if(req->documents[i])
				{
					std::string pdfname = std::to_string(page*resultsPerPage+i) + ".pdf";
					std::string jsonname = std::to_string(page*resultsPerPage+i) + ".json";
					if(!sci_save_document_to_file(req->documents[i], pdfname.c_str()))
					{
						Log(Log::INFO)<<"Could not save document "<<pdfname;
					}
					else
					{
						bool ret = document_meta_save(jsonname.c_str(), req->documents[i], NULL);
						if(!ret)
							Log(Log::WARN)<<"Could not save document metadata"<<jsonname;
					}
				}
				++processed;
			}

			request_return_free(req);
		}
	}

	return 0;
}
