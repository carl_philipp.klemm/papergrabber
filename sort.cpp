#include <filesystem>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <algorithm>
#include <scipaper/scipaper.h>

#include "log.h"
#include "nxjson.h"

const char* otherdir = "other";

const char* topics[] =
{
	"Batteries",
	"Biology",
	"Corrosion",
	"Capacitors",
	"FuleCells",
	"Photovoltaics",
	"Sensors",
	nullptr,
};

const char* topicKeywords[][32] =
{
	{"lithium-ion", "battery", "batteries", "lithium ion", "lithium polymer", nullptr},
	{"biochemistry", "biochemical", "biological", "biofilm", "biomedical", "patients",
		"living cell", "extracellular", "neuron", "bio-sensing", "bio-sensor",
		"biosensor", "cancer cell", "tissues",  "living systems", "pathogen", "diagnosis",
		"cellular", " rat ", "bioimpedance", "bioelectrochemical", " DNA ", nullptr},
	{"corrosion", "steel exposed", nullptr},
	{"capacitor", "dielectric material", "dielectric medium", nullptr},
	{"fuel cell", "fuel cells", "PEFC", "energy conversion",  nullptr},
	{"solar cell", "photovoltaic", "photoelectric", "solar cells", nullptr},
	{"piezoelectric", "development of sensor", "sensors", "sensing", nullptr},
	nullptr,
};

static bool prepareDirectories(const std::filesystem::path& outDir)
{
	if(!std::filesystem::is_directory(outDir))
	{
		Log(Log::INFO)<<outDir<<" dose not exist, createing";
		if(!std::filesystem::create_directory(outDir))
		{
			Log(Log::ERROR)<<"Could not create "<<outDir;
			return false;
		}
	}

	for(size_t i = 0; topics[i]; ++i)
	{
		if(!std::filesystem::is_directory(outDir/topics[i]) &&
			!std::filesystem::create_directory(outDir/topics[i]))
		{
			Log(Log::ERROR)<<"Could not create "<<outDir/topics[i];
			return false;
		}
	}

	if(!std::filesystem::is_directory(outDir/otherdir) &&
		!std::filesystem::create_directory(outDir/otherdir))
	{
		Log(Log::ERROR)<<"Could not create "<<outDir/otherdir;
		return false;
	}
	return true;
}

static size_t sortIntoTopic(const char* titleIn, const char* keywordsIn, const char* abstractIn)
{
	std::string title;
	if(titleIn)
	{
		title.assign(titleIn);
		std::transform(title.begin(), title.end(), title.begin(), [](unsigned char c){return std::tolower(c);});
	}

	std::string keywords;
	if(keywordsIn)
	{
		keywords.assign(keywordsIn);
		std::transform(keywords.begin(), keywords.end(), keywords.begin(), [](unsigned char c){return std::tolower(c);});
	}

	std::string abstract;
	if(abstractIn)
	{
		abstract.assign(abstractIn);
		std::transform(abstract.begin(), abstract.end(), abstract.begin(), [](unsigned char c){return std::tolower(c);});
	}

	size_t i = 0;
	if(!title.empty())
	{
		for(; topics[i]; ++i)
		{
			for(size_t j = 0; topicKeywords[i][j]; ++j)
			{
				if(keywords.find(topicKeywords[i][j]) != std::string::npos)
					return i;
			}
		}
	}

	i = 0;
	for(; topics[i]; ++i)
	{
		for(size_t j = 0; topicKeywords[i][j]; ++j)
		{
			if(title.find(topicKeywords[i][j]) != std::string::npos)
				return i;
		}
	}

	i = 0;
	for(; topics[i]; ++i)
	{
		for(size_t j = 0; topicKeywords[i][j]; ++j)
		{
			if(abstract.find(topicKeywords[i][j]) != std::string::npos)
				return i;
		}
	}
	return i;
}

int main(int argc, char** argv)
{
	Log::level = Log::WARN;
	if(argc < 2)
	{
		Log(Log::ERROR)<<"A directory path where the pdf and json files are is required";
		return 0;
	}

	std::filesystem::path outDir;
	if(argc > 3)
		outDir.assign(argv[2]);
	else
		outDir.assign("./out");
	Log(Log::INFO)<<"Saveing to "<<outDir;
	if(!prepareDirectories(outDir))
		return -1;

	const std::filesystem::path directoryPath(argv[1]);
	for(const std::filesystem::directory_entry& dirent : std::filesystem::directory_iterator{directoryPath})
	{
		std::filesystem::path pdfPath = dirent.path();
		std::filesystem::path jsonPath = pdfPath;
		jsonPath.replace_extension("json");
		if(pdfPath.extension() == ".pdf" && dirent.is_regular_file())
		{
			Log(Log::INFO)<<pdfPath.filename();
			if(std::filesystem::is_regular_file(jsonPath))
			{
				int jsonFd = open(jsonPath.string().c_str(), O_CLOEXEC, O_RDONLY);
				if(jsonFd < 0)
				{
					Log(Log::WARN)<<"Could not open "<<jsonPath;
					continue;
				}
				struct stat jsonStat;
				int ret = fstat(jsonFd, &jsonStat);
				if(ret < 0)
				{
					Log(Log::WARN)<<"Could not stat "<<jsonPath;
					close(jsonFd);
					continue;
				}

				char *jsonText = (char*)malloc(jsonStat.st_size+1);
				jsonText[jsonStat.st_size] = '\0';
				ret = read(jsonFd, jsonText, jsonStat.st_size);
				close(jsonFd);
				if(ret < jsonStat.st_size)
				{
					Log(Log::WARN)<<"Underlength read on "<<jsonPath;
					free(jsonText);
					continue;
				}

				const char* title = nullptr;
				const char* keywords = nullptr;
				const char* abstract = nullptr;
				const nx_json* json = nx_json_parse_utf8(jsonText);
				if(!json)
				{
					free(jsonText);
					continue;
				}
				const nx_json* abstractJson = nx_json_get(json, "abstract");
				const nx_json* keywordsJson = nx_json_get(json, "keywords");
				const nx_json* titleJson = nx_json_get(json, "title");
				if(abstractJson->type == NX_JSON_STRING)
				{
					abstract = abstractJson->text_value;
				}
				else
				{
					nx_json_free(json);
					free(jsonText);
					continue;
				}
				if(keywordsJson->type == NX_JSON_STRING)
					keywords = keywordsJson->text_value;
				if(titleJson->type == NX_JSON_STRING)
					title = titleJson->text_value;
				if(!keywords && !abstract && !title)
				{
					Log(Log::WARN)<<"Unable to get abstract, title or keywords in "<<jsonPath;
					nx_json_free(json);
					free(jsonText);
					std::filesystem::create_hard_link(pdfPath, outDir/otherdir/pdfPath.filename());
					std::filesystem::create_hard_link(jsonPath, outDir/otherdir/jsonPath.filename());
					continue;
				}

				std::filesystem::path hardLinkPath(outDir);
				size_t sortedId = sortIntoTopic(title, keywords, abstract);
				if(topics[sortedId] == nullptr)
					hardLinkPath = outDir/otherdir;
				else
					hardLinkPath = outDir/topics[sortedId];

				std::filesystem::create_hard_link(pdfPath, hardLinkPath/pdfPath.filename());
				std::filesystem::create_hard_link(jsonPath, hardLinkPath/jsonPath.filename());

				nx_json_free(json);
				free(jsonText);
			}
			else
			{
				Log(Log::WARN)<<pdfPath<<" dose not have a corrisponding json file";
			}
		}
	}
	return 0;
}

