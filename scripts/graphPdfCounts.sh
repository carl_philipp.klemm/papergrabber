#!/bin/bash

data=""
scriptdir=$(dirname "$0")

declare -i sum=0

for dir in $1/*; do
    [ -d "${dir}" ] || continue
    declare -i lineCount=$(ls $dir/*pdf | wc -l)
    lineCount=lineCount
    sum+=lineCount
    data+=$lineCount
    data+=$(echo -n ", "$(basename $dir))
    data+=$'\n'
done

declare -i i=0

sotedData=$(sort -n <<< "$data")

finalData=""

IFS='
'
for line in $sotedData; do
    finalData+=$(printf "%i, %s" $i "${line[@]}")
    finalData+=$'\n'
    i+=1
done

printf "sum: %i\n" $sum
printf "%s\n" "${finalData[@]}"
gnuplot $scriptdir/barPlot.p -p <<< $finalData
